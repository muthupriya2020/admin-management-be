const express = require('express');
const studentController = require('../controllers/studentController');
const router = express.Router();
//create router
router.post('/create', studentController.createStudent);

//edit router
 router.post('/edit',studentController.editStudent);

 //list router
 router.get('/list',studentController.listStudent);

 //delete
 router.post('/delete',studentController.deleteStudent);


 
module.exports = router;
