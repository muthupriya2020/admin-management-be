const express = require('express');
const courseController = require('../controllers/courseController');
const router = express.Router();
//create router
router.post('/create', courseController.createCourse);

//edit router
 router.post('/edit',courseController.editCourse);

 //list router
 router.get('/list',courseController.listCourses);

 //delete
 router.post('/delete',courseController.deleteCourse);

module.exports = router;
