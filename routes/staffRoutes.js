const express = require('express');
const staffController = require('../controllers/staffController');
const router = express.Router();
//create router
router.post('/create', staffController.createStaff);

//edit router
 router.post('/edit',staffController.editStaff);

 //list router
 router.get('/list',staffController.listStaff);

 //delete
 router.post('/delete',staffController.deleteStaff);

module.exports = router;