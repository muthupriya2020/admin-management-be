const express = require("express");
const morgan = require("morgan");
const rateLimit = require("express-rate-limit");
const helmet = require("helmet");
const mongoSanitize = require("express-mongo-sanitize");
const xss = require("xss-clean");
const compression = require("compression");
const cors = require("cors");
const unless = require("express-unless");
const path = require("path");
const { upload, s3 } = require("./helpers/AWSFileUpload");
const { v4: uuidv4 } = require("uuid");
const authMiddleware = require("./middleware/auth");
const globalErrorHandler = require("./controllers/errorController");
const courseRoutes = require("./routes/courseRoutes");
const staffRoutes =require("./routes/staffRoutes")
const studentRoutes =require("./routes/studentRoutes")
// Start express app
const app = express();

app.enable("trust proxy");

// GLOBAL MIDDLEWARES
app.use(cors());
// Serving static files
app.use("/images", express.static(path.join(__dirname, "images")));
// Middleware for authenticating token submitted with requests
authMiddleware.authenticateToken.unless = unless;
app.use(
  authMiddleware.authenticateToken.unless({
    path: [
      { url: "/api/v1/course/create", methods: ["POST"] },
      { url: "/api/v1/course/edit", methods: ["POST"] },
      { url: "/api/v1/course/delete", methods: ["POST"] },
      { url: "/api/v1/course/list", methods: ["GET"] },
      { url: "/api/v1/staff/create", methods: ["POST"] },
      { url: "/api/v1/staff/edit", methods: ["POST"] },
      { url: "/api/v1/staff/delete", methods: ["POST"] },
      { url: "/api/v1/staff/list", methods: ["GET"] },
      { url: "/api/v1/student/create", methods: ["POST"] },
      { url: "/api/v1/student/edit", methods: ["POST"] },
      { url: "/api/v1/student/delete", methods: ["POST"] },
      { url: "/api/v1/student/list", methods: ["GET"] },
      { url: "/images", methods: ["GET"] },

    ],
  })
);

// Set security HTTP headers
app.use(helmet());

// Development logging
if (process.env.NODE_ENV === "development") {
  app.use(morgan("dev"));
}

// Limit requests from same API
const limiter = rateLimit({
  max: 1000,
  windowMs: 60 * 60 * 1000,
  message: "Too many requests from this IP, please try again in an hour!",
});
// app.use("/api", limiter);

app.use("/", express.static(path.join(__dirname, "images")));

// Body parser, reading data from body into req.body

app.use(express.json({ limit: "20mb" }));
app.use(
  express.urlencoded({ extended: true, limit: "20mb", parameterLimit: "5000" })
);

// Data sanitization against NoSQL query injection
app.use(mongoSanitize());

// Data sanitization against XSS
app.use(xss());

// use compresssion
app.use(compression());

// set the view engine to ejs
app.set("view engine", "ejs");
app.use(express.static("views"));

// Test middleware
app.use((req, res, next) => {
  req.requestTime = new Date().toISOString();
  // console.log(req);
  next();
});

// ROUTES
app.use("/api/v1/course", courseRoutes);
app.use("/api/v1/staff",staffRoutes);
app.use("/api/v1/student",studentRoutes);

// app.use('/api/v1/library', libraryRoutes);

// app.use("/api/v1/test", (req, res, next) => {
//   res.status(200).json({ status: "Success" });
// });
app.post("/api/v1/upload", upload, (req, res) => {
  let myFile = req.file.originalname.split(".");

  const fileType = myFile[myFile.length - 1];

  const { type } = req.body;
  const folderName = type || "profile";
  // console.log(fileType);

  const params = {
    Bucket: process.env.AWS_BUCKETNAME + `/${folderName}`,
    Key: `${uuidv4()}.${fileType}`,
    Body: req.file.buffer,
    ContentType: fileType,
  };

  // console.log(params);

  s3.upload(params, (error, data) => {
    if (error) {
      res.status(500).send(error);
    }

    res.status(200).send({ data: data, status: true, msg: "success" });
  });
});

// app.all('*', (req, res, next) => {
//   next(new AppError(`Can't find ${req.originalUrl} on this server!`, 404));
// });

app.use(globalErrorHandler);

module.exports = app;
