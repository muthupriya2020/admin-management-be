const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const CourseSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  id: {
    type: String,
    required: true,
    
  },
  date: {
    type: Date,
    default: Date.now
  },
  instructor: {
    type: String
  },
  credits:{
    type: String,
    required: true
  },
  fee:{
    type: Number,
    required: true 
  },
  duration: {
    type: Array,
    required: true
  },
  topics: {
    type: String,
    required: true
  },
  description: {
    type: String,
    required: true
  },
},{timestamps:true});

module.exports = mongoose.model('Course', CourseSchema);