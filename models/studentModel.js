const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const StudentSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  courseId: {
    type: String,
    required: true
  },
  staffId: {
    type: String,
    required: true
  },
  dateOfBirth: {
    type: Date,
    required: true
  },
  dateEnrolled: {
    type: Date,
    default: Date.now
  },
  image:{
    type: String,
    required: true
  },
  phone:{
    type:Number,
    required:true
  },
  studentId:{
    type: String,
    required: true
  },
  address:{
    type: String,
    required: true
  },
 
},{timestamps:true});

module.exports = mongoose.model('Student', StudentSchema);
