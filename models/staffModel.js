const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const StaffSchema = new Schema({
  name: {
    type: String,
    required: true
  },
  email: {
    type: String,
    required: true,
    unique: true
  },
  phone: {
    type: Number,
    required: true
  },
  courseId: {
    type: String,
    required: true
  },
  dateJoined: {
    type: Date,
    default: Date.now
  },
  staffId :{
    type:String,
    required:true
  },
  salary:{
    type:Number
  },
  studentId:{
    type: Array,
  
  }
},{timestamps:true});

module.exports = mongoose.model('Staff', StaffSchema);
