const Utils = require("../helpers/utils");
const APIRes = require("../helpers/result");
const { validationResult } = require("express-validator");
const studentModel = require('../models/studentModel')
const Joi = require('joi');
const { sendEmail } = require("../helpers/mailer");
//create course
const multer =require('multer')
var storage = multer.diskStorage({
    destination: function (req, file, cb) {
      cb(null, './images')
    },
    filename: function (req, file, cb) {
      cb(null, "student" + '-' + Date.now() + file.originalname)
    }
  })
  var upload = multer({ storage : storage,limits:{fileSize:10000000000000}}).single('image');
exports.createStudent = async (req, res, next) => {
    try {
      // let errors = validationResult(req);
      // if (!errors.isEmpty()) {
      //   throw errors.array();
      // }
      // const userInput = Utils.getReqValues(req);
      // // Validation schema for the form
      // const formSchema = Joi.object().keys({
      //   email: Joi.string().email().required(),
      //   dateEnrolled: Joi.date().required(),
      //   dateOfBirth: Joi.date().required(),
      //   name:Joi.string().min(5).max(50).required(),
      //   phone: Joi.string().regex(/^[0-9]+$/).min(10).max(15).required(),
      //   courseId:Joi.string().required(),
      //   staffId:Joi.string().required(),
      //   studentId:Joi.string().required(),
      //   address: Joi.string().min(10).max(200).required(),
      //   // image:Joi.string().required(),
      // });
      // const findData = await studentModel.findOne({ id: userInput.id })
      // if (findData) {
      //   return APIRes.getErrorResult("Course Id already exist", res);
      // } else {
      //   // Validate the form data
      //   const result = formSchema.validate(userInput);
      //   if (result.error) {
      //   // Return the validation error
      //     res.status(400).send(result.error.details[0].message);
      //   } else {
      //   // save the course to the database
      //     let msg =`Hi ${userInput.name}, this is to notify you that we have
      //     updated your details successfully for the ${userInput.courseId} course.
      //     THANK YOU.`
      
          upload(req,res,async function(err) {
            if(err) {
               
                return res.end("Error uploading file.");
            }
           try {
            let userInput = Utils.getReqValues(req);
            userInput.image = ("http://localhost:5000/images/"+req?.file.filename);
            // console.log(userInput)
           
          //  const createdData = await productModel.create(userInput);
          //        return APIRes.getMessageResult(createdData, "success", res);
         

                  const createData = await studentModel.create(userInput)
                  console.log(createData);
                  return APIRes.getSuccessResult("Course created successfully", res)
           } catch (error) {
            return APIRes.getErrorResult(error, res);
           }
          // const body_html = await ejs.renderFile("./views/email/email.ejs", { name: findData.email, siteurl: process.env.SITE_URL, token: msg });
          // emailParam = { senderAddress: process.env.MAIL_FROM_NAME + "<" + process.env.MAIL_FROM_ADDRESS + ">", toAddress: findData.email, subject: "Confirmation Email", body_html: body_html };
          // let response = await sendEmail(emailParam);
          // return APIRes.getSuccessResult("Student created successfully", res)
           });
         
          // const createData =await studentModel.create(userInput)
          // const body_html = await ejs.renderFile("./views/email/email.ejs", { name: findData.email, siteurl: process.env.SITE_URL, token: msg });
          // emailParam = { senderAddress: process.env.MAIL_FROM_NAME + "<" + process.env.MAIL_FROM_ADDRESS + ">", toAddress: findData.email, subject: "Confirmation Email", body_html: body_html };
          // let response = await sendEmail(emailParam);
          // return APIRes.getSuccessResult("Course created successfully", res)
        // }
      // }
    } catch (error) {
      return APIRes.getErrorResult(error, res);
    }
  }

//list student
exports.listStudent = async(req,res,next)=>{
    try {
      let errors = validationResult(req);
      if (!errors.isEmpty()) {
        throw errors.array();
      }
      const userInput = Utils.getReqValues(req);
      const find = await studentModel.find().select('name email phone courseId studentId staffId image dateEnrolled dateOfBirth address').lean();
      if (!find) {
        return APIRes.getErrorResult("course not found", res);
      } else {
        return APIRes.getSuccessResult(find, res)
      }
    } catch (error) {
      console.log(error);
      return APIRes.getErrorResult(error, res);
    }
   }

//edit student
exports.editStudent=async(req,res,next)=>{
    try {
      let errors = validationResult(req);
      if (!errors.isEmpty()) {
        throw errors.array();
      }
      let userInput = Utils.getReqValues(req);
      const formSchema = Joi.object().keys({
        email: Joi.string().email().required(),
        dateEnrolled: Joi.date().required(),
        dateOfBirth: Joi.date().required(),
        name:Joi.string().min(5).max(50).required(),
        phone: Joi.string().regex(/^[0-9]+$/).min(10).max(15).required(),
        studentId:Joi.string().required(),
      });
      const find = await studentModel.findOne({id: userInput.id });
      if(find){
        const result = formSchema.validate(userInput);
        if (result.error) {
        // Return the validation error
          res.status(400).send(result.error.details[0].message);
        } else {
        // save the user to the database
          delete userInput.studentId
          const Data =await studentModel.findOneAndUpdate({studentId:userInput.studentId},{$set:userInput},{new:true})
          return APIRes.getSuccessResult(Data, res)
        }
      }else{
        return APIRes.getErrorResult("course not found", res);
      }
    } catch (error) {
      console.error(error);
      return APIRes.getErrorResult(error, res);
    }
  }

//delete Student
exports.deleteStudent=async(req,res,next)=>{
    try {
      let errors = validationResult(req);
      if (!errors.isEmpty()) {
        throw errors.array();
      }
      let userInput = Utils.getReqValues(req);
      const formSchema = Joi.object().keys({
        studentId:Joi.string().required(),
      });
      const find = await studentModel.findOne({id: userInput.id });
      if(find){
        const result = formSchema.validate(userInput);
        if (result.error) {
        // Return the validation error
          res.status(400).send(result.error.details[0].message);
        } else {
        // save the user to the database
          const Data =await studentModel.findOneAndDelete({studentId:userInput.stu4})
          return APIRes.getSuccessResult("student deleted successfully", res)
        }
      }else{
        return APIRes.getErrorResult("student not found", res);
      }
    } catch (error) {
      console.error(error);
      return APIRes.getErrorResult(error, res);
    }
  }