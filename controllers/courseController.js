const Utils = require("../helpers/utils");
const APIRes = require("../helpers/result");
const { validationResult } = require("express-validator");
const courseModel = require('../models/courseModel')
const Joi = require('joi');

//create course
exports.createCourse = async (req, res, next) => {
    try {
      let errors = validationResult(req);
      if (!errors.isEmpty()) {
        throw errors.array();
      }
      const userInput = Utils.getReqValues(req);
      // Validation schema for the form
      console.log(userInput);
      const formSchema = Joi.object().keys({
        topics: Joi.string().min(5).max(50).required(),
        description: Joi.string().min(10).max(200).required(),
        instructor: Joi.string().min(3).max(100).required(),
        date: Joi.date().required(),
        credits :Joi.number().min(1).max(3).required(),
        id:Joi.string().required(),
        fee:Joi.number().required(),
        duration:Joi.array(),
        name:Joi.string().min(5).max(50).required()
      });
      const findData = await courseModel.findOne({ id: userInput.id })
      if (findData) {
        return APIRes.getErrorResult("Course Id already exist", res);
      } else {
        // Validate the form data
        const result = formSchema.validate(userInput);
        if (result.error) {
        // Return the validation error
          res.status(400).send(result.error.details[0].message);
        } else {
        // save the course to the database
          const createData =await courseModel.create(userInput)
          return APIRes.getSuccessResult("Course created successfully", res)
        }
      }
    } catch (error) {
      return APIRes.getErrorResult(error, res);
    }
  }

//list courses
exports.listCourses = async(req,res,next)=>{
    try {
      let errors = validationResult(req);
      if (!errors.isEmpty()) {
        throw errors.array();
      }
      const userInput = Utils.getReqValues(req);
      const find = await courseModel.find().select('name topics description instructor date credits id duration fee').lean();
      if (!find) {
        return APIRes.getErrorResult("course not found", res);
      } else {
        return APIRes.getSuccessResult(find, res)
      }
    } catch (error) {
      console.log(error);
      return APIRes.getErrorResult(error, res);
    }
   }

//edit course
exports.editCourse=async(req,res,next)=>{
    try {
      let errors = validationResult(req);
      if (!errors.isEmpty()) {
        throw errors.array();
      }
      let userInput = Utils.getReqValues(req);
      const formSchema = Joi.object().keys({
        instructor: Joi.string().min(3).max(100).required(),
        id:Joi.string().required(),
      });
      const find = await courseModel.findOne({id: userInput.id });
      if(find){
        const result = formSchema.validate(userInput);
        if (result.error) {
        // Return the validation error
          res.status(400).send(result.error.details[0].message);
        } else {
        // save the user to the database
          const Data =await courseModel.findOneAndUpdate({id:userInput.id},{$set:{instructor:userInput.instructor}},{new:true})
          return APIRes.getSuccessResult(Data, res)
        }
      }else{
        return APIRes.getErrorResult("course not found", res);
      }
    } catch (error) {
      console.error(error);
      return APIRes.getErrorResult(error, res);
    }
  }

//delete course
exports.deleteCourse=async(req,res,next)=>{
    try {
      let errors = validationResult(req);
      if (!errors.isEmpty()) {
        throw errors.array();
      }
      let userInput = Utils.getReqValues(req);
      const formSchema = Joi.object().keys({
        id:Joi.string().required(),
      });
      const find = await courseModel.findOne({id: userInput.id });
      if(find){
        const result = formSchema.validate(userInput);
        if (result.error) {
        // Return the validation error
          res.status(400).send(result.error.details[0].message);
        } else {
        // save the user to the database
          const Data =await courseModel.findOneAndDelete({id:userInput.id})
          return APIRes.getSuccessResult("Course deleted successfully", res)
        }
      }else{
        return APIRes.getErrorResult("course not found", res);
      }
    } catch (error) {
      console.error(error);
      return APIRes.getErrorResult(error, res);
    }
  }