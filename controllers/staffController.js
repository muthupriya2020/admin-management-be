const Utils = require("../helpers/utils");
const APIRes = require("../helpers/result");
const { validationResult } = require("express-validator");
const staffModel = require('../models/staffModel')
const Joi = require('joi');

//create staff
exports.createStaff = async (req, res, next) => {
    try {
      let errors = validationResult(req);
      if (!errors.isEmpty()) {
        throw errors.array();
      }
      const userInput = Utils.getReqValues(req);
      // Validation schema for the form
      const formSchema = Joi.object().keys({
    name: Joi.string().min(3).max(100).required(),
    email: Joi.string().email().required(),
    salary: Joi.number().required(),
    dateOfJoin: Joi.date().required(),
    phone: Joi.string().regex(/^[0-9]+$/).min(10).max(15).required(),
    courseId:Joi.string().required(),
    staffId:Joi.string().required()
      });
      const findData = await staffModel.findOne({ staffId: userInput.staffId,courseId:userInput.courseId })
      if (findData) {
        return APIRes.getErrorResult("Staff Id or Course Id already exist", res);
      } else {
        // Validate the form data
        const result = formSchema.validate(userInput);
        if (result.error) {
        // Return the validation error
          res.status(400).send(result.error.details[0].message);
        } else {
        // save the course to the database
          const createData =await staffModel.create(userInput)
          return APIRes.getSuccessResult("Staff id created successfully", res)
        }
      }
    } catch (error) {
      return APIRes.getErrorResult(error, res);
    }
  }

//list staff
exports.listStaff = async(req,res,next)=>{
    try {
      let errors = validationResult(req);
      if (!errors.isEmpty()) {
        throw errors.array();
      }
      const userInput = Utils.getReqValues(req);
      const find = await staffModel.find().select('name email salary dateOfJoin phone staffId courseId studentId').lean();
      if (!find) {
        return APIRes.getErrorResult("staff not found", res);
      } else {
        return APIRes.getSuccessResult(find, res)
      }
    } catch (error) {
      console.log(error);
      return APIRes.getErrorResult(error, res);
    }
   }

//edit staff
exports.editStaff=async(req,res,next)=>{
    try {
      let errors = validationResult(req);
      if (!errors.isEmpty()) {
        throw errors.array();
      }
      let userInput = Utils.getReqValues(req);
      const formSchema = Joi.object().keys({
        studentId: Joi.string().min(3).max(100).required(),
        staffId:Joi.string().required(),
      });
      const find = await staffModel.findOne({id: userInput.id });
      if(find){
        const result = formSchema.validate(userInput);
        if (result.error) {
        // Return the validation error
          res.status(400).send(result.error.details[0].message);
        } else {
        // save the user to the database
          const Data =await staffModel.findOneAndUpdate({staffId:userInput.staffId},{$push:{studentId:userInput.studentId}},{new:true})
          return APIRes.getSuccessResult(Data, res)
        }
      }else{
        return APIRes.getErrorResult("course not found", res);
      }
    } catch (error) {
      console.error(error);
      return APIRes.getErrorResult(error, res);
    }
  }

//delete staff
exports.deleteStaff=async(req,res,next)=>{
    try {
      let errors = validationResult(req);
      if (!errors.isEmpty()) {
        throw errors.array();
      }
      let userInput = Utils.getReqValues(req);
      const formSchema = Joi.object().keys({
        staffId:Joi.string().required(),
      });
      const find = await staffModel.findOne({id: userInput.id });
      if(find){
        const result = formSchema.validate(userInput);
        if (result.error) {
        // Return the validation error
          res.status(400).send(result.error.details[0].message);
        } else {
        // save the user to the database
          const Data =await staffModel.findOneAndDelete({staffId:userInput.staffId})
          return APIRes.getSuccessResult("Staff deleted successfully", res)
        }
      }else{
        return APIRes.getErrorResult("staff not found", res);
      }
    } catch (error) {
      console.error(error);
      return APIRes.getErrorResult(error, res);
    }
  }